#include "stdafx.h"
#include "Square.h"

// Constructor with a type
Square::Square(Type type) : type(type) {
}

// Set Type
void Square::SetType(Type type) {
    this->type = type;
}

// Get Type
Square::Type Square::GetType() const {
    return type;
}

// Get Label
char Square::GetLabel() const {
    return TypeToLabel(type);
}

// LabelToType
const Square::Type Square::LabelToType(char label) {
    static const std::unordered_map<char, Type> map = {
        { '.', Type::Normal },
        { 'W', Type::Water },
        { 'R', Type::Rock },
        { 'B', Type::Barrier },
        { 'T', Type::Teleport },
        { 'L', Type::Lava }
    };
    auto item = map.find(label);
    if (item == map.end())
        throw std::invalid_argument("invalid label");
    return item->second;
}

// TypeToLabel
const char Square::TypeToLabel(Type type) {
    static const std::unordered_map<Type, char> map = {
        { Type::Normal, '.' },
        { Type::Water, 'W' },
        { Type::Rock, 'R' },
        { Type::Barrier, 'B' },
        { Type::Teleport, 'T' },
        { Type::Lava, 'L' }
    };
    auto item = map.find(type);
    if (item == map.end())
        throw std::invalid_argument("invalid type");
    return item->second;
}
