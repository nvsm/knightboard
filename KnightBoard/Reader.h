#pragma once
#include "Board.h"

// Board reader
class BoardReader
{
public:
    // Read board from stream
    Board Read(std::istream& stream);
};
