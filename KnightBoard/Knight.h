#pragma once
#include "Board.h"

class Knight {
public:
    // Constructor with board
    Knight(const Board& board);

    // Is valid position
    bool IsValidPosition(const Position& position) const;

    // Is valid move
    bool IsValidMove(const Position& start, const Position& end) const;

    // Is not a knight move
    bool IsKnightMove(const Position& start, const Position& end) const;

    // Is valid moves
    bool IsValidMoves(const std::vector<Position>& moves,
                      bool print = false) const;

    // Get minimum number of moves
    std::vector<Position> GetMinimumMoves(const Position& start,
                                          const Position& end) const;

    // Get label
    static char GetLabel();

private:
    // Get moves
    std::vector<Position> GetMoves(const Position start) const;

    // Get type distance
    int GetTypeDistance(const Position& position) const;

    // Get distance
    int GetDistance(const Position& from, const Position to) const;

    // Get move positions
    std::vector<std::vector<Position>>
        GetMovePositionsList(const Position& start,
                             const Position& end) const;

    // Get move sequence
    std::vector<Position> GetMoveSequence(
        const std::vector<std::vector<Position>>& prev,
        Position end) const;

private:
    // Board
    const Board& board;
};