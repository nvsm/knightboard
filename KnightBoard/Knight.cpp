#include "stdafx.h"
#include "Knight.h"
#include "Writer.h"

// Constructor with board
Knight::Knight(const Board& board) : board(board) {
}

// Is valid position
bool Knight::IsValidPosition(const Position& position) const {
    // If not in board
    if (!board.IsInside(position)) return false;

    // If rock
    if (board.GetSquare(position).GetType() == Square::Type::Rock)
        return false;

    // If barrier
    if (board.GetSquare(position).GetType() == Square::Type::Rock)
        return false;

    return true;
}

// Is valid move
bool Knight::IsValidMove(const Position& start, const Position& end) const {
    // If valid position
    if (!IsValidPosition(start) || !IsValidPosition(end)) return false;

    // If not a knight move
    if (!IsKnightMove(start, end)) return false;

    // If barrier
    auto positionsList = GetMovePositionsList(start, end);
    int barrierCount = 0;
    for (const auto& positions : positionsList)
        for (const auto& position : positions)
            if (board.GetSquare(position).GetType() == Square::Type::Barrier) {
                ++barrierCount;
                break;
            }

    return barrierCount != 2;
}

// Is not a knight move
bool Knight::IsKnightMove(const Position& start, const Position& end) const {
    int y = std::abs(start.y - end.y);
    int x = std::abs(start.x - end.x);
    return (x == 1 && y == 2) || (x == 2 && y == 1);
}

// Is valid moves
bool Knight::IsValidMoves(const std::vector<Position>& positions,
                          bool print) const {
    // Check for empty
    if (positions.empty()) return true;

    // Check first position
    if (!IsValidPosition(positions[0])) return false;

    // Check next positions
    for (size_t i = 1; i < positions.size(); ++i)
        if (!IsValidMove(positions[i - 1], positions[i])) return false;

    // Print moves
    if (print)
        for (const auto& position : positions) {
            PositionWriter(board, position).Write(std::cout);
            std::cout << '\n';
        }

    return true;
}

// Minimum number of moves
std::vector<Position> Knight::GetMinimumMoves(const Position& start,
                                              const Position& end) const {
    // Visited type
    using Visited = std::vector<std::vector<bool>>;
    // Prev type
    using Prev = std::vector<std::vector<Position>>;
    // Path type
    struct Path {
        Position position;
        int distance;
        bool operator<(const Path& path) const {
            return distance > path.distance;
        }
    };

    // Visited positions
    Visited visited(board.GetSize(), std::vector<bool>(board.GetSize()));
    // Prev positions
    Prev prev(board.GetSize(), std::vector<Position>(board.GetSize()));

    // Path queue
    std::priority_queue<Path> queue;

    // Insert start position into queue
    queue.push({ start, 0 });
    visited[start.y][start.x] = true;
    prev[start.y][start.x] = { -1, -1 };

    while (!queue.empty()) {
        // Get top from queue
        auto path = queue.top();
        queue.pop();

        // Check for end
        if (path.position == end) return GetMoveSequence(prev, end);

        // Add paths to queue
        for (const auto& next : GetMoves(path.position)) {
            // Check for visited
            if (visited[next.y][next.x]) continue;
            visited[next.y][next.x] = true;
            prev[next.y][next.x] = path.position;

            // Insert next into queue
            int distance = path.distance + GetDistance(path.position, next);
            queue.push({ next, distance });
        }
    }
    return std::vector<Position>();
}

// Get paths
std::vector<Position> Knight::GetMoves(const Position start) const {
    // Moves from zero
    static const std::vector<Position> zeroMoves = {
        { 1, 2 },{ -1, 2 },{ 1, -2 },{ -1, -2 },
        { 2, 1 },{ -2, 1 },{ 2, -1 },{ -2, -1 }
    };

    // Moves
    std::vector<Position> moves;

    for (const auto& zeroMove : zeroMoves) {
        auto move = start;

        // Set move position
        move.y += zeroMove.y;
        move.x += zeroMove.x;

        // If valid
        if (!IsValidMove(start, move))
            continue;

        // Add new move
        moves.push_back(move);
    }

    // Teleport case
    if (board.GetSquare(start).GetType() == Square::Type::Teleport)
        moves.push_back(board.GetSecondTeleport(start));

    return moves;
}

// Get distance for square type
int Knight::GetTypeDistance(const Position& position) const {
    static const std::unordered_map<Square::Type, int> costs = {
        {Square::Type::Normal, 1},
        {Square::Type::Teleport, 1 },
        {Square::Type::Water, 2},
        {Square::Type::Lava, 5}
    };

    // Get position type
    auto type = board.GetSquare(position).GetType();

    // Check type distance
    auto cost = costs.find(type);
    return cost != costs.end() ? cost->second : 0;
}

// Get distance
int Knight::GetDistance(const Position& from, const Position to) const {
    if (board.GetSquare(from).GetType() == Square::Type::Teleport) {
        Position second = board.GetSecondTeleport(from);
        if (second == to) return 0;
    }
    return GetTypeDistance(to);
}

// Get move positions
std::vector<std::vector<Position>>
Knight::GetMovePositionsList(const Position& start,
                             const Position& end) const {
    std::vector<std::vector<Position>> positions;
    // If not in board
    if (!board.IsInside(start) || !board.IsInside(end)) return positions;
    // If not a knight move
    if (!IsKnightMove(start, end)) return positions;

    // Generate positions
    // From Y
    positions.push_back(std::vector<Position>(1, start));
    Position position = start;
    while (position != end) {
        if (position.y != end.y) position.y += position.y < end.y ? 1 : -1;
        else position.x += position.x < end.x ? 1 : -1;
        positions[0].push_back(position);
    }
    // From X
    positions.push_back(std::vector<Position>(1, start));
    position = start;
    while (position != end) {
        if (position.x != end.x) position.x += position.x < end.x ? 1 : -1;
        else position.y += position.y < end.y ? 1 : -1;
        positions[1].push_back(position);
    }

    return positions;
}

// Get move sequence
std::vector<Position>
Knight::GetMoveSequence(const std::vector<std::vector<Position>>& prev,
                        Position end) const {
    std::vector<Position> sequence;
    for (; end != Position{ -1, -1 }; end = prev[end.y][end.x])
        sequence.push_back(end);
    std::reverse(sequence.begin(), sequence.end());
    return sequence;
}

// Get label
char Knight::GetLabel() {
    return 'K';
}