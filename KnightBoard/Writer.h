#pragma once
#include "Board.h"

// Position writer
class PositionWriter
{
public:
    // Constructor with board and position
    PositionWriter(const Board& board, const Position& position);

    // Write
    void Write(std::ostream& stream) const;

private:
    // Board
    const Board& board;
    // Position
    const Position& position;
};

