#include "stdafx.h"
#include "Board.h"

// Constructor by board size
Board::Board(int size)
    : board(size, std::vector<Square>(size)) {
}

// Get square
Square& Board::GetSquare(const Position& position) {
    return board[position.y][position.x];
}

// Get const square
const Square& Board::GetSquare(const Position& position) const {
    return board[position.y][position.x];
}

// Get board size
int Board::GetSize() const {
    return static_cast<int>(board.size());
}

// Is inside a board
bool Board::IsInside(const Position& position) const {
    return position.x >= 0 && position.x < GetSize()
        && position.y >= 0 && position.y < GetSize();
}

// Get second teleport
Position Board::GetSecondTeleport(const Position& first) const {
    Position second = { -1, -1 };
    if (GetSquare(first).GetType() != Square::Type::Teleport)
        throw std::invalid_argument("no first teleport");
    for (int i = 0; i < GetSize(); ++i)
        for (int j = 0; j < GetSize(); ++j) {
            Position second{ i, j };
            if (GetSquare(second).GetType() == Square::Type::Teleport &&
                first != second)
                return second;
        }
    throw std::exception("no second teleport");
}