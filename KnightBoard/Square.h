#pragma once

// Square
class Square {
public:
    // Type
    enum class Type {
        Normal,
        Water,
        Rock,
        Barrier,
        Teleport,
        Lava
    };

    // Constructor with a type
    Square(Type type = Type::Normal);

    // Set Type
    void SetType(Type type);

    // Get Type
    Type GetType() const;

    // Get Label
    char GetLabel() const;

    // LabelToType
    static const Type LabelToType(char label);

    // TypeToLabel
    static const char TypeToLabel(Type type);

private:
    // Type
    Type type;
};
