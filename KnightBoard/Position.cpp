#include "stdafx.h"
#include "Position.h"

// Is position equals
bool Position::operator==(const Position& position) const {
    return y == position.y && x == position.x;
}

// Is position not equals
bool Position::operator!=(const Position& position) const {
    return !operator==(position);
}