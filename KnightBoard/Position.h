#pragma once

// Position
struct Position
{
    // Y coordinate
    int y;
    // X coordinate
    int x;

    // Is position equals
    bool operator==(const Position& position) const;

    // Is position not equals
    bool operator!=(const Position& position) const;
};
