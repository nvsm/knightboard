#include "stdafx.h"
#include "Writer.h"
#include "Knight.h"

// Constructor with board and position
PositionWriter::PositionWriter(const Board& board,
                               const Position& position)
    : board(board), position(position) {
}

// Write
void PositionWriter::Write(std::ostream& stream) const {
    int size = board.GetSize();
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j)
            if (position.y == i && position.x == j)
                stream << Knight::GetLabel();
            else
                stream << board.GetSquare({ i, j }).GetLabel();
        stream << '\n';
    }
}