#include "stdafx.h"
#include "Reader.h"

// Read board from stream
Board BoardReader::Read(std::istream& stream) {
    int size;
    stream >> size;
    Board board(size);
    for (int i = 0; i < size * size; ++i) {
        Position position = { i / size, i % size };
        char label;
        stream >> label;
        board.GetSquare(position).SetType(Square::LabelToType(label));
    }
    return board;
}