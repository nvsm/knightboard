#pragma once
#include <vector>
#include "Square.h"
#include "Position.h"

// Board
class Board {
public:
    // Constructor by board size
    Board(int size = 0);

    // Get square
    Square& GetSquare(const Position& position);

    // Get const square
    const Square& GetSquare(const Position& position) const;

    // Get board size
    int GetSize() const;

    // Is inside a board
    bool IsInside(const Position& position) const;

    // Get Second Teleport
    Position GetSecondTeleport(const Position& first) const;

private:
    // Board
    std::vector<std::vector<Square>> board;
};