#include "stdafx.h"
#include "CppUnitTest.h"
#include "Position.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace KnightBoardTest {

    // Position test
    TEST_CLASS(PositionTest) {
public:
    TEST_METHOD_INITIALIZE(MethodInit) {
        position = Position{ 1, 2 };
    }

    // Equal
    TEST_METHOD(Equal) {
        Assert::IsTrue(position == Position{ 1, 2 });
    }

    // Not equal by x
    TEST_METHOD(NotEqualByX) {
        Assert::IsFalse(position == Position{ 2, 2 });
    }

    // Not equal by y
    TEST_METHOD(NotEqualByY) {
        Assert::IsFalse(position == Position{ 1, 1 });
    }

private:
    // Position
    Position position;
    };
}