#include "stdafx.h"
#include "CppUnitTest.h"
#include "CoutAssert.h"
#include "DataFile.h"
#include "Knight.h"
#include "Reader.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace KnightBoardTest {

    // Knight test
    TEST_CLASS(KnightTest) {
public:
    TEST_METHOD_INITIALIZE(MethodInit) {
        board = Board(8);
        knight.reset(new Knight(board));
    }

    // Valid moves
    TEST_METHOD(ValidMoves) {
        std::vector<Position> moves = { {0, 0}, {1, 2}, {3, 3} };
        Assert::IsTrue(knight->IsValidMoves(moves));
    }

    // Invalid moves in board
    TEST_METHOD(InvalidMovesInBoard) {
        std::vector<Position> moves = { { 0, 0 }, { 1, 2 }, { 3, 2 } };
        Assert::IsFalse(knight->IsValidMoves(moves));
    }

    // Invalid moves off the board
    TEST_METHOD(InvalidMovesOffBoard) {
        std::vector<Position> moves = { { 0, 0 }, { -1, 2 }, { 0, 0 } };
        Assert::IsFalse(knight->IsValidMoves(moves));
    }

    // Get minimum number of moves
    TEST_METHOD(GetMinimumMoves) {
        std::vector<Position> moves = { {2, 0}, {1, 2}, {0, 0} };
        AssertMoves(moves, "Normal.txt", { 2, 0 }, { 0, 0 });
    }

    // No path
    TEST_METHOD(NoPath) {
        std::vector<Position> moves;
        AssertMoves(moves, "Normal.txt", { 0, 0 }, { 1, 1 });
    }

    // Water
    TEST_METHOD(Water) {
        std::vector<Position> moves = { { 0, 0 }, { 1, 2 },{ 2, 0 } };
        AssertMoves(moves, "Water.txt", { 0, 0 }, { 2, 0 });
    }

    // Rock
    TEST_METHOD(Rock) {
        std::vector<Position> moves = { { 0, 0 }, { 2, 1 },{ 0, 2 },
            {1, 0}, {2, 2}, {0, 1}, {2, 0} };
        AssertMoves(moves, "Rock.txt", { 0, 0 }, { 2, 0 });
    }

    // Lava
    TEST_METHOD(Lava) {
        std::vector<Position> moves = { { 0, 0 }, { 1, 2 },{ 2, 0 } };
        AssertMoves(moves, "Lava.txt", { 0, 0 }, { 2, 0 });
    }

    // Barrier
    TEST_METHOD(Barrier) {
        std::vector<Position> moves = { { 0, 0 }, { 2, 1 },{ 3, 3 }, {1, 2} };
        AssertMoves(moves, "Barrier.txt", { 0, 0 }, { 1, 2 });
    }

    // Teleport
    TEST_METHOD(Teleport) {
        std::vector<Position> moves = { { 0, 0 },{ 1, 2 },{ 5, 6 },{ 7, 7 } };
        AssertMoves(moves, "Teleport.txt", { 0, 0 }, { 7, 7 });
    }

    // Board 32x32 size
    TEST_METHOD(Board32) {
        board = BoardReader().Read(DataFile("32x32.txt").Stream());
        knight.reset(new Knight(board));
        auto r = knight->GetMinimumMoves({ 0, 0 }, { 31, 31 });
        Assert::IsTrue(23 == r.size());
    }

    // Print moves
    TEST_METHOD(PrintMoves) {
        AssertPrintMoves("Normal.txt", "Normal_Moves.txt");
    }

    // Print moves with specific squares
    TEST_METHOD(PrintMovesWithSpecificSquares) {
        AssertPrintMoves("SpecificSquare.txt", "SpecificSquare_Moves.txt");
    }

private:
    // Assert print moves
    void AssertPrintMoves(const std::string& board, const std::string& moves) {
        std::stringstream s;
        s << DataFile(moves).Stream().rdbuf();

        auto f = [&data = board] {
            Board board = BoardReader().Read(DataFile(data).Stream());
            Knight knight(board);
            std::vector<Position> moves = { { 0, 0 },{ 1, 2 } };
            knight.IsValidMoves(moves, true);
        };
        CoutAssert::AreEqual(s, f);
    }

    // Assert number of moves
    void AssertMoves(std::vector<Position> moves,
                     const std::string file,
                     const Position& start, const Position& end) {
        board = BoardReader().Read(DataFile(file).Stream());
        knight.reset(new Knight(board));
        auto r = knight->GetMinimumMoves(start, end);
        Assert::IsTrue(moves == r);
    }


private:
    // Knight
    std::unique_ptr<Knight> knight;
    // Board
    Board board;
    };
}