#include "stdafx.h"
#include "CppUnitTest.h"
#include "Board.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace KnightBoardTest {

    // Board test
    TEST_CLASS(BoardTest) {
public:
    TEST_METHOD_INITIALIZE(MethodInit) {
        board = Board(8);
    }

    // Board size
    TEST_METHOD(GetSize) {
        Assert::AreEqual(8, board.GetSize());
    }

    // Square inside
    TEST_METHOD(SquareInside) {
        Assert::IsTrue(board.IsInside({ 0, 0 }));
    }

    // Top square overflow
    TEST_METHOD(TopSquareOverflow) {
        Assert::IsFalse(board.IsInside({ -1, 0 }));
    }

    // Right square overflow
    TEST_METHOD(RightSquareOverflow) {
        Assert::IsFalse(board.IsInside({ 0, 8 }));
    }

    // Bottom square overflow
    TEST_METHOD(BottomSquareOverflow) {
        Assert::IsFalse(board.IsInside({ 8, 0 }));
    }

    // Left square overflow
    TEST_METHOD(LeftSquareOverflow) {
        Assert::IsFalse(board.IsInside({ 0, -1 }));
    }

private:
    // Board
    Board board;
    };

}