#pragma once
#include <fstream>
#include <filesystem>

// Data file
class DataFile
{
public:
    // Constructor with path
    DataFile(const std::string& path) {
        namespace fs = std::experimental::filesystem;
        this->path = (fs::path(__FILE__).remove_filename() / path).string();
        stream.open(this->path);
    }

    // Stream
    auto& Stream() {
        return stream;
    }

    // Path
    const auto& Path() const {
        return path;
    }

protected:
    std::ifstream stream;
    std::string path;
};
