#pragma once
#include <iostream>

// std::cout redirect
class CoutRedirect {
public:
    CoutRedirect(std::streambuf* buffer)
        : old(std::cout.rdbuf(buffer)) {
    }
    CoutRedirect() {
        std::cout.rdbuf(old);
    }

private:
    std::streambuf* old;
};