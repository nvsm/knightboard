#include "stdafx.h"
#include "CppUnitTest.h"
#include "Square.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace KnightBoardTest {

    // Square test
    TEST_CLASS(SquareTest) {
public:
    TEST_METHOD_INITIALIZE(MethodInit) {
        square = Square();
    }

    // Get default type
    TEST_METHOD(GetDefaultType) {
        Assert::IsTrue(Square::Type::Normal == square.GetType());
    }

    // Get specific type
    TEST_METHOD(GetSpecificType) {
        square = Square(Square::Type::Water);
        Assert::IsTrue(Square::Type::Water == square.GetType());
    }

    // Get default label
    TEST_METHOD(GetDefaultLabel) {
        Assert::AreEqual('.', square.GetLabel());
    }

    // Get specific label
    TEST_METHOD(GetSpecificLabel) {
        square = Square(Square::Type::Water);
        Assert::AreEqual('W', square.GetLabel());
    }

    // Get specific label by type
    TEST_METHOD(GetSpecificLabelByType) {
        Assert::AreEqual('W', Square::TypeToLabel(Square::Type::Water));
    }

    // Get specific type by label
    TEST_METHOD(GetSpecificTypeByLabel) {
        Assert::IsTrue(Square::Type::Water == Square::LabelToType('W'));
    }

    // Unknown label
    TEST_METHOD(UnknownLabel) {
        Assert::ExpectException<std::invalid_argument>(
            [] {Square::LabelToType('U'); }
        );
    }

private:
    // Square
    Square square;
    };
}